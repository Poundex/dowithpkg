package net.poundex.dowithpkg.core.rule

import com.fasterxml.jackson.databind.ObjectMapper
import io.vavr.CheckedFunction1
import net.poundex.dowithpkg.core.RuntimeConfiguration
import net.poundex.dowithpkg.core.io.IO
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.NoSuchFileException
import java.nio.file.Path
import java.nio.file.Paths

class RulesetManagerSpec extends Specification {

	private static final Path mainConfigDir = Paths.get("/config/dir")
	private static final RuntimeConfiguration runtimeConfiguration = 
		new RuntimeConfiguration(mainConfigDir)
	
	private static final String rulesetJson = """
{
	"file1": [
		{"SomeRule": {"var": "val1"}},
		{"SomeRule": {"var": "val2"}}
	],
	
	"file2": [
		{"SomeRule": {"var": "val3"}},
		{"AnotherRule": {"var": "val4", "othervar": "val5"}}
	]
}
"""
	PreparedRule someRulePrepared = Stub()
	PreparedRule anotherRulePrepared = Stub()
	Rule someRule = Stub() {
		prepare(_) >> someRulePrepared
	}
	Rule anotherRule = Stub() {
		prepare(_) >> anotherRulePrepared
	}
	
	Ruleset expected(String rulesetName) {
		return new Ruleset(rulesetName,
			Map.of(
				"file1", [someRulePrepared, someRulePrepared],
				"file2", [someRulePrepared, anotherRulePrepared]))
	}

	RuleManager ruleManager = Stub() {
		findByName("SomeRule") >> Optional.of(someRule)
		findByName("AnotherRule") >> Optional.of(anotherRule)
	}
	
	IO io = Mock()
	
	@Subject
	RulesetManager rulesetManager = new RulesetManager(
		runtimeConfiguration, io, new ObjectMapper(), ruleManager)
	
	void "Returns empty when looking up by package name and does not exist"() {
		when:
		Optional<Ruleset> ruleset = rulesetManager.findRuleset("does-not-exist")
		
		then:
		1 * io.exists(mainConfigDir.resolve("rules/").resolve("does-not-exist.json"))
		
		and:
		ruleset.empty
	}
	
	void "Returns empty when looking up by path and does not exist"() {
		when:
		rulesetManager.findRuleset("does-not-exist.json")

		then:
		1 * io.exists(Paths.get("does-not-exist.json")) >> false
		thrown(RulesetFileNotFoundException.class)
	}
	
	void "Returns ruleset from package name"() {
		given:
		Path target = mainConfigDir.resolve("rules/").resolve("packagename.json")
		
		when:
		Optional<Ruleset> ruleset = rulesetManager.findRuleset("packagename")

		then:
		1 * io.exists(target) >> true
		1 * io.withReader(target, _) >> { Path p, CheckedFunction1<Reader, ?> f -> 
			f.apply(new StringReader(rulesetJson)) }
		
		and:
		ruleset.get() == expected("packagename")
	}
	
	void "Returns ruleset from path"() {
		given:
		io.exists(Paths.get("/an/explicit/file.json")) >> true
		
		when:
		Optional<Ruleset> ruleset = rulesetManager.findRuleset("/an/explicit/file.json")

		then:
		1 * io.withReader(Paths.get("/an/explicit/file.json"), _) >> { Path p, CheckedFunction1<Reader, ?> f -> 
			f.apply(new StringReader(rulesetJson)) }
		
		and:
		ruleset.get() == expected("file")
	}
}

