package net.poundex.dowithpkg.core.rule.rules

import net.poundex.dowithpkg.core.io.IO
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path

class ContentSpec extends Specification {

	IO io = Mock()

	@Subject
	Content rule = new Content(io)

	Path targetFile = Stub()

	void "Writes content to file"() {
		when:
		rule.prepare([
			Content: "some content"])
			.apply(targetFile)

		then:
		1 * io.writeString(targetFile, "some content")
	}
}
