package net.poundex.dowithpkg.core.rule

import org.springframework.beans.factory.ObjectProvider
import spock.lang.Specification
import spock.lang.Subject

import java.util.stream.Stream

class RuleManagerSpec extends Specification {

	private static class TestRule implements Rule {
		@Override
		PreparedRule prepare(Map<String, Object> args) {
			throw new UnsupportedOperationException()
		}
	}
	
	Rule testRule = new TestRule()

	ObjectProvider<Rule> rules = Stub() {
		stream() >> { Stream.of(testRule) }
	}

	@Subject
	RuleManager ruleManager = new RuleManager(rules)

	void setup() {
		ruleManager.onApplicationEvent(null)
	}
	
	void "Returns rule if found or empty"() {
		expect:
		ruleManager.findByName("TestRule").get() == testRule
		ruleManager.findByName("FakeRule").empty
	}
}
