package net.poundex.dowithpkg.core.rule.rules

import net.poundex.dowithpkg.core.io.IO
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path

class DeleteSpec extends Specification {

	IO io = Mock()

	@Subject
	Delete rule = new Delete(io)

	Path targetFile = Stub()

	void "Deletes matching line"() {
		given:
		io.readString(targetFile) >> """\
a line
another line
delete this line!
last line"""

		when:
		rule.prepare([
			Match: "this line"])
			.apply(targetFile)

		then:
		1 * io.writeString(targetFile, """\
a line
another line
last line""")
	}
}
