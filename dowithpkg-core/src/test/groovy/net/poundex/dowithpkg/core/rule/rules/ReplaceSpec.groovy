package net.poundex.dowithpkg.core.rule.rules


import net.poundex.dowithpkg.core.io.IO
import net.poundex.dowithpkg.core.rule.PreparedRule
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path

class ReplaceSpec extends Specification {
	
	private static final String input = """\
One
find
Two
find find
Three
"""
	
	private static final String expectedOutput = """\
One
replace
Two
replace replace
Three
"""
	
	IO io = Mock()
	
	@Subject
	Replace rule = new Replace(io)

	Path targetFile = Stub()
	
	void "Executes replacements in file contents"() {
		when:
		PreparedRule preparedRule = rule.prepare([
			Find: "find", 
			Replace: "replace"])
		preparedRule.apply(targetFile)
		
		then:
		1 * io.readString(targetFile) >> input
		1 * io.writeString(targetFile, expectedOutput)
	}
}
