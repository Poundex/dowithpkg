package net.poundex.dowithpkg.core.rule.rules

import net.poundex.dowithpkg.core.process.ProcessExecutor
import net.poundex.dowithpkg.core.process.ProcessExecutorFactory
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.nio.file.Paths

class PatchFileSpec extends Specification {

	ProcessExecutor processExecutor = Mock()
	ProcessExecutorFactory processExecutorFactory = Stub() {
		get() >> { processExecutor }
	}
	
	@Subject
	PatchFile rule = new PatchFile(processExecutorFactory)

	Path targetFile = Paths.get("/path/to/target")

	void "Applies given patch to file"() {
		when:
		rule.prepare([
			File: "/path/to/patch"])
			.apply(targetFile)

		then:
		1 * processExecutor.executeTask(null, ["patch", "/path/to/target", "/path/to/patch"])
	}
}
