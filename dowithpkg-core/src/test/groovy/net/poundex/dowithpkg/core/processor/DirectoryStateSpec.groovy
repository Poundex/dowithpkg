package net.poundex.dowithpkg.core.processor

import spock.lang.Specification
import spock.lang.Subject

class DirectoryStateSpec extends Specification {

	@Subject
	DirectoryState directoryState = DirectoryState.create([
		nomatch: "OLDHASH",
		match  : "RIGHTHASH"
	])

	void "Should process when no existing hash"() {
		expect:
		directoryState.shouldProcess("none", null)
	}

	void "Should process when existing hash does not match"() {
		expect:
		directoryState.shouldProcess("nomatch", { "NEWHASH" })
	}

	void "Should not process when existing hash matches"() {
		expect:
		! directoryState.shouldProcess("match", { "RIGHTHASH" })
	}
}
