package net.poundex.dowithpkg.core.rule.rules

import net.poundex.dowithpkg.core.io.IO
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path

class ArrayPushSpec extends Specification {

	IO io = Mock()

	@Subject
	ArrayPush rule = new ArrayPush(io)

	Path targetFile = Stub()

	void "Adds elements to inline arrays"() {
		given:
		io.readString(targetFile) >> "arch=('i686' 'x86_64')"

		when:
		rule.prepare([
			Array  : "arch",
			Element: "aarch64"])
			.apply(targetFile)

		then:
		1 * io.writeString(targetFile, 
			"arch=('i686' 'x86_64' 'aarch64')")
	}
	
	void "Adds elements to multi line arrays"() {
		given:
		io.readString(targetFile) >> """\
depends=(
  'pacman>5'
  'git'
)"""
		
		when: 
		rule.prepare([
			Array  : "depends",
			Element: "somepackage"])
			.apply(targetFile)

		then:
		1 * io.writeString(targetFile,
			"""\
depends=(
  'pacman>5'
  'git'
  'somepackage'
)""")
	}
	
	void "Adds elements to empty arrays"() {
		given:
		io.readString(targetFile) >> "optdepends=()"

		when:
		rule.prepare([
			Array  : "optdepends",
			Element: "anotherpackage"])
			.apply(targetFile)

		then:
		1 * io.writeString(targetFile,
			"optdepends=('anotherpackage')")
	}
	
	void "Adds elements to new arrays"() {
		given:
		io.readString(targetFile) >> ""

		when:
		rule.prepare([
			Array  : "makedepends",
			Element: "yetanotherpackage"])
			.apply(targetFile)

		then:
		1 * io.writeString(targetFile,
			"makedepends=('yetanotherpackage')")
	}
}
