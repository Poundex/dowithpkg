package net.poundex.dowithpkg.core.rule.rules

import net.poundex.dowithpkg.core.io.IO
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path

class InsertSpec extends Specification {

	IO io = Mock()

	@Subject
	Insert rule = new Insert(io)

	Path targetFile = Stub()

	void "Inserts above match"() {
		given:
		io.readString(targetFile) >> """\
a line
another line
match this line!
last line"""

		when:
		rule.prepare([
			Match: "this line",
			Above: "inserted text"
		])
			.apply(targetFile)

		then:
		1 * io.writeString(targetFile, """\
a line
another line
inserted text
match this line!
last line""")
	}
	
	void "Inserts below match"() {
		given:
		io.readString(targetFile) >> """\
a line
another line
match this line!
last line"""

		when:
		rule.prepare([
			Match: "this line",
			Below: "inserted text"
		])
			.apply(targetFile)

		then:
		1 * io.writeString(targetFile, """\
a line
another line
match this line!
inserted text
last line""")
	}
}
