package net.poundex.dowithpkg.core.processor

import net.poundex.dowithpkg.core.io.IO
import net.poundex.dowithpkg.core.rule.PreparedRule
import net.poundex.dowithpkg.core.rule.Ruleset
import net.poundex.dowithpkg.core.rule.RulesetManager
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.stream.Stream

class DirectoryProcessorSpec extends Specification {
	
	private static final Path cwd = Paths.get(System.getProperty("user.dir"))
	
	RulesetManager rulesetManager = Mock()
	FileStateService fileStateService = Mock()
	
	Path stageDir = Paths.get("/stage/dir")
	IO io = Mock()
	PreparedRule preparedRule1 = Mock()
	PreparedRule preparedRule2 = Mock()
	
	@Subject
	DirectoryProcessor directoryProcessor = 
		new DirectoryProcessor(rulesetManager, io, fileStateService)

	void "Handle not finding ruleset"() {
		when:
		ProcessorResult result = directoryProcessor.processDirectory(
			Optional.of("someruleset"),
			Optional.empty())

		then:
		1 * rulesetManager.findRuleset("someruleset") >> Optional.empty()
	}
	
	void "Process given ruleset and providedWorkDir"(
		Optional<String> providedRulesetName, 
		String rulesetName,
		Optional<Path> providedWorkDir,
		Path workDir) {

		given:
		fileStateService.getDirectoryState(workDir) >> DirectoryState.create([
			file1: "FILE1OLDHASH",
			file3: "DONOTPROCESS"
		])
		io.hash(workDir.resolve("file1")) >> "FILE1NEWHASH"
		io.hash(workDir.resolve("file2")) >> "FILE2NEWHASH"
		io.hash(workDir.resolve("file3")) >> "DONOTPROCESS"

		when:
		ProcessorResult result = directoryProcessor.processDirectory(providedRulesetName, providedWorkDir)

		then:
		1 * rulesetManager.findRuleset(rulesetName) >> Optional.of(new Ruleset(rulesetName, Map.of(
			"file1", [preparedRule1],
			"file2", [preparedRule1, preparedRule2],
			"file3", [preparedRule1]
		)))
		1 * io.createTempDirectory(_ as String) >> stageDir
		1 * io.copy(workDir.resolve("file1"), stageDir.resolve("file1"))
		1 * io.copy(workDir.resolve("file2"), stageDir.resolve("file2"))
		0 * io.copy(workDir.resolve("file3"), _)
		1 * preparedRule1.apply(stageDir.resolve("file1"))
		1 * preparedRule1.apply(stageDir.resolve("file2"))
		1 * preparedRule2.apply(stageDir.resolve("file2"))
		0 * preparedRule1.apply(stageDir.resolve("file3"))
		
		and:
		result.workDir() == workDir
		result.stageDir() == stageDir
		result.processed()
		result.rulesetName() == rulesetName
		result.filesProcessed() == [file1: true, file2: true, file3: false]

		where:
		providedRulesetName        | rulesetName                 | providedWorkDir                           | workDir 
		Optional.empty()           | cwd.getFileName().toString()| Optional.empty()                          | cwd
		Optional.empty()           | "packagedir"                | Optional.of(Paths.get("/tmp/packagedir")) | Paths.get("/tmp/packagedir")
		Optional.of("someruleset") | "someruleset"               | Optional.empty()                          | cwd
		Optional.of("someruleset") | "someruleset"               | Optional.of(Paths.get("/tmp/packagedir")) | Paths.get("/tmp/packagedir")
	}
	
	void "Writes result to work dir"() {
		given:
		Path workDir = Paths.get("/work/dir")
		Path stageDir = Paths.get("/stage/dir")
		Path stagedFile1 = stageDir.resolve("PKGBUILD")
		Path stagedFile2 = stageDir.resolve("nested/file")
		
		ProcessorResult processorResult = new ProcessorResult(
			workDir, stageDir, true, null, null)

		when:
		directoryProcessor.writeProcessed(processorResult)

		then:
		1 * io.walk(stageDir) >> { Stream.of(stagedFile1, stagedFile2) }
		
		1 * io.ensureOwningDirectory(workDir.resolve("PKGBUILD"))
		1 * io.copy(stagedFile1, workDir.resolve("PKGBUILD"), StandardCopyOption.REPLACE_EXISTING)

		1 * io.ensureOwningDirectory(workDir.resolve("nested/file"))
		1 * io.copy(stagedFile2, workDir.resolve("nested/file"), StandardCopyOption.REPLACE_EXISTING)
	}
}
