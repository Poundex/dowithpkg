package net.poundex.dowithpkg.core.rule.rules

import net.poundex.dowithpkg.core.io.IO
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.nio.file.Paths

class ContentFileSpec extends Specification {

	IO io = Mock()

	@Subject
	ContentFile rule = new ContentFile(io)

	Path targetFile = Stub()

	void "Writes content to file"() {
		given:
		io.readString(Paths.get("/path/to/content")) >> "some content"
		
		when:
		rule.prepare([
			File: "/path/to/content"])
			.apply(targetFile)

		then:
		1 * io.writeString(targetFile, "some content")
	}
}
