package net.poundex.dowithpkg.core.rule.rules

import net.poundex.dowithpkg.core.io.IO
import net.poundex.dowithpkg.core.rule.PreparedRule
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path

class ReplaceCommandSpec extends Specification {

	IO io = Mock()
	
	@Subject
	ReplaceCommand rule = new ReplaceCommand(io)

	Path targetFile = Stub()

	void "Replace simple command"() {
		given:
		io.readString(targetFile) >>  """\
line
  line
  oldcommand --one --two --three
line"""
		when:
		PreparedRule preparedRule = rule.prepare([
			Find   : "oldcommand",
			Replace: "newcommand --four --five"])
		preparedRule.apply(targetFile)

		then:
		1 * io.writeString(targetFile, """\
line
  line
  newcommand --four --five
line""")
	}

	void "Replace command with preamble"() {
		given:
		io.readString(targetFile) >>  """\
line
  line
  FOO=BAR /path/to/oldcommand --one --two --three
line"""
		when:
		PreparedRule preparedRule = rule.prepare([
			Find   : "oldcommand",
			Replace: "newcommand --four --five"])
		preparedRule.apply(targetFile)

		then:
		1 * io.writeString(targetFile, """\
line
  line
  FOO=BAR /path/to/newcommand --four --five
line""")
	}

	void "Replace multiline command"() {
		given:
		io.readString(targetFile) >>  """\
line
  line
  oldcommand --one \\\\
  --two \\\\
  --three
line"""
		when:
		PreparedRule preparedRule = rule.prepare([
			Find   : "oldcommand",
			Replace: "newcommand --four --five"])
		preparedRule.apply(targetFile)

		then:
		1 * io.writeString(targetFile, """\
line
  line
  newcommand --four --five
line""")
	}
}
