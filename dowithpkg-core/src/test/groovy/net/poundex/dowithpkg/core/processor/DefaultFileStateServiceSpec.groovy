package net.poundex.dowithpkg.core.processor

import io.vavr.CheckedFunction1
import net.poundex.dowithpkg.core.io.IO
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.nio.file.Paths

class DefaultFileStateServiceSpec extends Specification {
	
	Path workDir = Paths.get("/work/dir")
	Path stageDir = Paths.get("/stage/dir")
	
	IO io = Mock()
	
	@Subject
	FileStateService fileHashService = new DefaultFileStateService(io)

	void "Writes hashes for processed files when no existing hashes found"() {
		given:
		BufferedWriter w = Mock() 
		io.hash(workDir.resolve("file1")) >> "F1HASH"
		io.hash(workDir.resolve("file2")) >> "F2HASH"
		
		and:
		ProcessorResult processorResult = new ProcessorResult(
			workDir,
			stageDir,
			true,
			null,
			[file1: true, file2: true, file3: false])
		
		when:
		fileHashService.writeFileState(processorResult)
		
		then:
		1 * io.withWriter(workDir.resolve(".dowithpkg"), _) >> { Path p, CheckedFunction1<Writer, ?> fn ->
			fn.apply(w)
		}
		1 * w.write("file1=F1HASH")
		1 * w.write("file2=F2HASH")
		0 * w.write({ it.contains("file3") })
	}
	
	void "Updates existing hashes"() {
		given:
		BufferedWriter w = Mock()
		io.hash(workDir.resolve("file1")) >> "F1HASH"
		io.hash(workDir.resolve("file2")) >> "F2HASH"
		
		and:
		io.exists(workDir.resolve(".dowithpkg")) >> true
		io.withReader(workDir.resolve(".dowithpkg"), _) >> { Path p, CheckedFunction1<Reader, ?> fn ->
			fn.apply(new StringReader("untouchedfile=UNTOUCHEDHASH"))
		}

		and:
		ProcessorResult processorResult = new ProcessorResult(
			workDir,
			stageDir,
			true,
			null,
			[file1: true, file2: true, file3: false, untouchedfile: false])

		when:
		fileHashService.writeFileState(processorResult)

		then:
		1 * io.withWriter(workDir.resolve(".dowithpkg"), _) >> { Path p, CheckedFunction1<Writer, ?> fn ->
			fn.apply(w)
		}
		1 * w.write("file1=F1HASH")
		1 * w.write("file2=F2HASH")
		1 * w.write("untouchedfile=UNTOUCHEDHASH")
	}
	
	void "Returns empty state"() {
		given:
		Path stateFile = workDir.resolve(".dowithpkg")
		io.exists(stateFile) >> false
		
		when:
		Properties state = fileHashService.getDirectoryState(workDir).getProperties()
		
		then:
		state.isEmpty()
	}
	
	void "Returns existing state"() {
		given:
		Path stateFile = workDir.resolve(".dowithpkg")
		io.exists(stateFile) >> true
		io.withReader(stateFile, _) >> { Path p, CheckedFunction1<Reader, ?> fn ->
			fn.apply(new StringReader("""\
file1=FILE1HASH
file2=FILE2HASH
"""))
		}

		when:
		Properties state = fileHashService.getDirectoryState(workDir).getProperties()

		then:
		state['file1'] == "FILE1HASH"
		state['file2'] == "FILE2HASH"
	}
}
