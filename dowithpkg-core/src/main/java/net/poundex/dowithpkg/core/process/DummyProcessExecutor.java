package net.poundex.dowithpkg.core.process;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.output.Output;

import java.nio.file.Path;
import java.util.Arrays;

@RequiredArgsConstructor
class DummyProcessExecutor implements ProcessExecutor {
	
	private final Output output;
	
	@Override
	public void executeUser(String... command) {
		output.out(String.format("Exec %s", Arrays.toString(command)));
	}

	@Override
	public void executeTask(Path dir, String... command) {
		output.out(String.format("Exec %s %s", dir, Arrays.toString(command)));
	}
}
