package net.poundex.dowithpkg.core.rule.rules;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.io.IO;
import net.poundex.dowithpkg.core.rule.PreparedRule;
import net.poundex.dowithpkg.core.rule.Rule;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@Component
public class ArrayPush implements Rule {
	
	private final IO io;
	
	@Override
	public PreparedRule prepare(Map<String, Object> args) {
		return path -> arrayPush(
			path,
			checkArg(args.get("Array"), "Array name"),
			checkArg(args.get("Element"), "Array element"));
	}

	private void arrayPush(Path path, String arrayName, String arrayElement) {
		Pattern pattern = Pattern.compile(
			arrayName + "=\\((.*)\\)", Pattern.DOTALL);

		String input = io.readString(path);
		Matcher matcher = pattern.matcher(input);
		io.writeString(path, matcher.find()
			? matcher.replaceFirst(mr -> {
				boolean nl = mr.group(1).contains("\n");
				return String.format("%s=(%s%s'%s'%s)",
					arrayName,
					mr.group(1).stripTrailing(),
					nl ? "\n  " : (mr.group(1).isEmpty() ? "" : " "),
					arrayElement,
					nl ? "\n" : "");
			})
			: String.format("%s=('%s')", arrayName, arrayElement));
	}
}
