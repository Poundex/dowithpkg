package net.poundex.dowithpkg.core.rule;

import java.nio.file.Path;

public interface PreparedRule {
	void apply(Path path);
}
