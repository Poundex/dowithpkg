package net.poundex.dowithpkg.core.rule.rules;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.io.IO;
import net.poundex.dowithpkg.core.rule.PreparedRule;
import net.poundex.dowithpkg.core.rule.Rule;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@Component
public class Content implements Rule {

	private final IO io;

	@Override
	public PreparedRule prepare(Map<String, Object> args) {
		return path -> writeContent(
			path,
			checkArg(args.get("Content"), "File content"));
	}

	private void writeContent(Path path, String fileContent) {
		io.writeString(path, fileContent);
	}
}
