package net.poundex.dowithpkg.core.rule;

import java.util.List;
import java.util.Map;

public record Ruleset(String name, Map<String, List<? extends PreparedRule>> fileRules) {
}
