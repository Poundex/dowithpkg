package net.poundex.dowithpkg.core.rule.rules;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.io.IO;
import net.poundex.dowithpkg.core.rule.PreparedRule;
import net.poundex.dowithpkg.core.rule.Rule;
import org.springframework.stereotype.Component;

import java.util.Map;

@RequiredArgsConstructor
@Component
public class Replace implements Rule {
	
	private final IO io;
	
	@Override
	public PreparedRule prepare(Map<String, Object> args) {
		return path -> io.writeString(path,
			io.readString(path).replaceAll(
				checkArg(args.get("Find"), "Find string"),
				checkArg(args.get("Replace"), "Replace string")));
	}
}
