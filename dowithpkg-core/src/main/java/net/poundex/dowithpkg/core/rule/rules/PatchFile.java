package net.poundex.dowithpkg.core.rule.rules;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.process.ProcessExecutorFactory;
import net.poundex.dowithpkg.core.rule.PreparedRule;
import net.poundex.dowithpkg.core.rule.Rule;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

@RequiredArgsConstructor
@Component
public class PatchFile implements Rule {
	
	private final ProcessExecutorFactory processExecutorFactory;
	
	@Override
	public PreparedRule prepare(Map<String, Object> args) {
		return path -> applyPatch(
			path,
			Paths.get((String) checkArg(args.get("File"), "Path to patch file")));
	}

	private void applyPatch(Path targetFile, Path patchFile) {
		processExecutorFactory.get().executeTask(null, 
			"patch", 
			targetFile.toAbsolutePath().toString(),
			patchFile.toAbsolutePath().toString());
	}
}
