package net.poundex.dowithpkg.core.process;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.output.Output;

import java.util.function.BooleanSupplier;

@RequiredArgsConstructor
public class ProcessExecutorFactory {
	
	private final BooleanSupplier noExecSupplier;
	private final Output output;
	
	private ProcessExecutor last;
	
	public ProcessExecutor get() {
		if(last != null)
			return last;
		
		return noExecSupplier.getAsBoolean()
			? new DummyProcessExecutor(output)
			: new ProcessExecutorImpl();
	}
}
