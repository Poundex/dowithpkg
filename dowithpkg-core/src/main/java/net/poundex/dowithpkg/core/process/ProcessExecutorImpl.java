package net.poundex.dowithpkg.core.process;


import io.vavr.control.Try;
import net.poundex.dowithpkg.core.exception.ClientException;

import java.nio.file.Path;
import java.util.Arrays;

class ProcessExecutorImpl implements ProcessExecutor {
	
	@Override
	public void executeUser(String... command) {
		ProcessBuilder processBuilder = new ProcessBuilder(command).inheritIO();
		Try.run(() -> processBuilder.start().waitFor());
	}

	@Override
	public void executeTask(Path dir, String... command) {
		ProcessBuilder processBuilder = new ProcessBuilder(command)
			.inheritIO()
			.directory(dir.toFile());
		
		if(Try.of(() -> processBuilder.start().waitFor()).get() != 0)
			throw new ClientException(String.format("Command %s in directory %s failed", 
				Arrays.toString(command), 
				dir));
	}
}
