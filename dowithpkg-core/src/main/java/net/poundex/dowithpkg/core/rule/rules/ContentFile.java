package net.poundex.dowithpkg.core.rule.rules;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.io.IO;
import net.poundex.dowithpkg.core.rule.PreparedRule;
import net.poundex.dowithpkg.core.rule.Rule;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

@RequiredArgsConstructor
@Component
public class ContentFile implements Rule {
	
	private final IO io;
	
	@Override
	public PreparedRule prepare(Map<String, Object> args) {
		return path -> writeContent(
			path,
			Paths.get((String) checkArg(args.get("File"), "Content file")));
	}

	private void writeContent(Path path, Path contentFie) {
		io.writeString(path, io.readString(contentFie));
	}

}
