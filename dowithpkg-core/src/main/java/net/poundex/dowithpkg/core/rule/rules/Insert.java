package net.poundex.dowithpkg.core.rule.rules;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.exception.ClientException;
import net.poundex.dowithpkg.core.io.IO;
import net.poundex.dowithpkg.core.rule.PreparedRule;
import net.poundex.dowithpkg.core.rule.Rule;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Component
public class Insert implements Rule {
	
	private final IO io;

	@Override
	public PreparedRule prepare(Map<String, Object> args) {
		String match = checkArg(args.get("Match"), "Search pattern");
		if (args.containsKey("Above")) 
			return path -> insertAbove(
				path,
				match,
				checkArg(args.get("Above"), "Text to insert"));
		else if (args.containsKey("Below"))
			return path -> insertBelow(
				path,
				match,
				checkArg(args.get("Below"), "Text to insert"));
		else
			throw new ClientException("Missing text to insert (Above or Below)");
	}

	private void insertAbove(Path path, String match, String text) {
		doInsert(path, match, text, 0);
	}
	
	private void insertBelow(Path path, String match, String text) {
		doInsert(path, match, text, 1);
	}

	private void doInsert(Path path, String match, String text, int offset) {
		Predicate<String> matches = Pattern.compile(match).asPredicate();
		List<String> lines = io.readString(path).lines().toList();
		IntStream.range(0, lines.size())
			.filter(idx -> matches.test(lines.get(idx)))
			.findFirst()
			.ifPresent(idx -> io.writeString(path,
				Stream.concat(
						lines.subList(0, idx + offset).stream(),
						Stream.concat(
							Stream.of(text),
							lines.subList(idx + offset, lines.size()).stream()))
					.collect(Collectors.joining("\n"))));
	}
}
