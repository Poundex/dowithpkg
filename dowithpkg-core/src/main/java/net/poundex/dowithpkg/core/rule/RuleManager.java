package net.poundex.dowithpkg.core.rule;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
class RuleManager implements ApplicationListener<ContextRefreshedEvent> {
	
	private final ObjectProvider<Rule> rules;
	private Map<String, Rule> ruleMap;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent ignored) {
		ruleMap = rules.stream().collect(Collectors.toMap(
			kv -> kv.getClass().getSimpleName(),
			kv -> kv));
	}
	
	public Optional<Rule> findByName(String name) {
		return Optional.ofNullable(ruleMap.get(name));
	}
}
