package net.poundex.dowithpkg.core.output;

import org.springframework.stereotype.Component;

@Component
class DefaultOutput implements Output {
	
	@Override
	public void out(String string) {
		System.out.println(string);
	}
}
