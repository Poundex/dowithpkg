package net.poundex.dowithpkg.core.io;

import io.vavr.CheckedFunction1;

import java.io.Reader;
import java.io.Writer;
import java.nio.file.CopyOption;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface IO {
	Path createTempDirectory(String prefix);
	boolean exists(Path path);
	<T> T withReader(Path path, CheckedFunction1<Reader, T> fn);
	Path copy(Path source, Path destination, CopyOption... copyOptions);
	<T> T withWriter(Path path, CheckedFunction1<Writer, T> fn);
	String readString(Path path);
	void writeString(Path path, String string);
	void ensureOwningDirectory(Path path);
	Stream<Path> walk(Path path);
	String hash(Path path);
}
