package net.poundex.dowithpkg.core.processor;

import java.nio.file.Path;

public interface FileStateService {
	void writeFileState(ProcessorResult processorResult);
	DirectoryState getDirectoryState(Path workDir);
}
