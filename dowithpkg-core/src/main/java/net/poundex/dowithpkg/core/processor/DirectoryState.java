package net.poundex.dowithpkg.core.processor;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;

@RequiredArgsConstructor
public class DirectoryState {
	
	public static DirectoryState create(Map<String, String> map) {
		Properties p = new Properties();
		p.putAll(map);
		return new DirectoryState(p);
	}
	
	@Getter(AccessLevel.PROTECTED)
	private final Properties properties;

	public boolean shouldProcess(String filename, Supplier<String> hashSupplier) {
		if( ! properties.containsKey(filename))
			return true;
		
		return ! hashSupplier.get().equals(properties.getProperty(filename));
	}
}
