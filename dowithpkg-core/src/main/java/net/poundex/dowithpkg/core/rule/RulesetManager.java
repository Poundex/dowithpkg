package net.poundex.dowithpkg.core.rule;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.RuntimeConfiguration;
import net.poundex.dowithpkg.core.io.IO;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class RulesetManager {
	
	private final RuntimeConfiguration runtimeConfiguration;
	private final IO io;
	private final ObjectMapper objectMapper;
	private final RuleManager ruleManager;

	public Optional<Ruleset> findRuleset(String rulesetName) {
		if(rulesetName.contains(".") || rulesetName.contains("/"))
			return findRulesetByPath(rulesetName);
		
		return findRulesetByPackageName(rulesetName);
	}

	private Optional<Ruleset> findRulesetByPath(String rulesetName) {
		Path rulesetFile = Paths.get(rulesetName);
		if( ! io.exists(rulesetFile))
			throw new RulesetFileNotFoundException(rulesetFile);
		
		return Optional.of(getRuleset(rulesetFile));
	}

	private Optional<Ruleset> findRulesetByPackageName(String rulesetName) {
		Path search = runtimeConfiguration.configDir()
			.resolve("rules/")
			.resolve(rulesetName + ".json");

		if(io.exists(search))
			return Optional.of(getRuleset(search));
		
		return Optional.empty();
	}

	private Ruleset getRuleset(Path path) {
		return new Ruleset(
			path.getFileName().toString().replace(".json", ""),
			
			io.withReader(path, r -> objectMapper.readValue(r,
					new TypeReference<Map<String, List<Map<String, Map<String, Object>>>>>() {})
				.entrySet()
				.stream()
				.collect(Collectors.toMap(
					Map.Entry::getKey,
					kv -> kv.getValue().stream()
						.map(this::toRule)
						.toList()))));
	}

	private PreparedRule toRule(Map<String, Map<String, Object>> map) {
		Map.Entry<String, Map<String, Object>> ruleMap = map.entrySet().iterator().next();
		return ruleManager.findByName(ruleMap.getKey())
			.map(r -> r.prepare(ruleMap.getValue()))
			.orElseThrow();
	}
}
