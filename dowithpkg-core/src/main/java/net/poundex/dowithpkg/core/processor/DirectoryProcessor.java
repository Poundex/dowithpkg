package net.poundex.dowithpkg.core.processor;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.io.IO;
import net.poundex.dowithpkg.core.rule.PreparedRule;
import net.poundex.dowithpkg.core.rule.Ruleset;
import net.poundex.dowithpkg.core.rule.RulesetManager;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

@RequiredArgsConstructor
@Service
public class DirectoryProcessor {
	
	private final RulesetManager rulesetManager;
	private final IO io;
	private final FileStateService fileStateService;
	
	public ProcessorResult processDirectory(
		Optional<String> rulesetName, 
		Optional<Path> workDir) {

		Path resolvedWorkDir = workDir.orElseGet(() -> Paths.get(System.getProperty("user.dir")));

		return rulesetManager.findRuleset(rulesetName
				.orElseGet(() -> resolvedWorkDir.getFileName().toString()))
			.map(r -> doProcessDirectory(resolvedWorkDir, r))
			.orElse(new ProcessorResult(resolvedWorkDir, null, false, null, null));
	}

	private ProcessorResult doProcessDirectory(Path workDir, Ruleset ruleset) {
		Path stageDir = io.createTempDirectory("dowithpkg");
		DirectoryState directoryState = fileStateService.getDirectoryState(workDir);
		Map<String, Boolean> processedFiles = new HashMap<>(ruleset.fileRules().size());
		
		ruleset.fileRules().forEach((f, rs) -> 
			processedFiles.put(f, 
				processFile(workDir, stageDir, directoryState, f, rs)));
		
		return new ProcessorResult(
			workDir,
			stageDir,	
			true,	
			ruleset.name(),	
			processedFiles);
	}

	private boolean processFile(
		Path workDir,
		Path stageDir, 
		DirectoryState directoryState, 
		String filename, 
		List<? extends PreparedRule> rules) {
		
		if( ! directoryState.shouldProcess(filename, () -> io.hash(workDir.resolve(filename))))
			return false;
		
		Path target = stageDir.resolve(filename);
		io.ensureOwningDirectory(target);
		io.copy(workDir.resolve(filename), target);

		rules.forEach(rule -> rule.apply(target));

		return true;
	}

	public void writeProcessed(ProcessorResult processorResult) {
		io.walk(processorResult.stageDir())
			.forEach(sf -> {
				if(Files.isDirectory(sf))
					return;
				
				Path tf = processorResult.workDir().resolve(processorResult.stageDir().relativize(sf));
				io.ensureOwningDirectory(tf);
				io.copy(sf, tf, StandardCopyOption.REPLACE_EXISTING);
			});
	}
}
