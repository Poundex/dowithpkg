package net.poundex.dowithpkg.core.io;

import io.vavr.CheckedFunction1;
import io.vavr.control.Try;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.io.Reader;
import java.io.Writer;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

@Service
class DefaultIO implements IO {
	@Override
	public Path createTempDirectory(String prefix) {
		return Try.of(() -> Files.createTempDirectory(prefix)).get();
	}

	@Override
	public boolean exists(Path path) {
		return Files.exists(path);
	}

	@Override
	public <T> T withReader(Path path, CheckedFunction1<Reader, T> fn) {
		return Try.withResources(() -> Files.newBufferedReader(path))
			.of(fn)
			.get();
	}

	@Override
	public Path copy(Path source, Path destination, CopyOption... copyOptions) {
		return Try.of(() -> Files.copy(source, destination, copyOptions)).get();
	}
	
	@Override
	public <T> T withWriter(Path path, CheckedFunction1<Writer, T> fn) {
		return Try.withResources(() -> Files.newBufferedWriter(path))
			.of(fn)
			.get();
	}

	@Override
	public String readString(Path path) {
		return Try.of(() -> Files.readString(path)).get();
	}

	@Override
	public void writeString(Path path, String string) {
		Try.of(() -> Files.writeString(path, string)).get();
	}

	@Override
	public void ensureOwningDirectory(Path path) {
		Try.of(() -> Files.createDirectories(path.getParent())).get();
	}

	@Override
	public Stream<Path> walk(Path path) {
		return Try.of(() -> Files.walk(path)).get();
	}

	@Override
	public String hash(Path path) {
		return Try.withResources(() -> Files.newInputStream(path))
			.of(DigestUtils::md5DigestAsHex)
			.get();
	}
}
