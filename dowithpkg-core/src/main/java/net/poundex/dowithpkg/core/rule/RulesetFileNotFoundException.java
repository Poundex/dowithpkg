package net.poundex.dowithpkg.core.rule;

import net.poundex.dowithpkg.core.exception.ClientException;

import java.nio.file.Path;

public class RulesetFileNotFoundException extends ClientException {
	
	public RulesetFileNotFoundException(String file) {
		super(String.format("Explicitly named ruleset file %s not found", file));
	}

	public RulesetFileNotFoundException(Path file) {
		this(file.toAbsolutePath().toString());
	}
}
