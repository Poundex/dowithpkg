package net.poundex.dowithpkg.core.rule;

import java.util.Map;
import java.util.Objects;

public interface Rule {
	PreparedRule prepare(Map<String, Object> args);
	
	default <T> T checkArg(Object object, String msg) {
		return (T) Objects.requireNonNull(object, msg);
	}
}
