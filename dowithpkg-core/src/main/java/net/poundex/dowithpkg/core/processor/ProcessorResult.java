package net.poundex.dowithpkg.core.processor;

import java.nio.file.Path;
import java.util.Map;
import java.util.Properties;

public record ProcessorResult(
	Path workDir, 
	Path stageDir, 
	boolean processed,
	String rulesetName,
	Map<String, Boolean> filesProcessed) { }
