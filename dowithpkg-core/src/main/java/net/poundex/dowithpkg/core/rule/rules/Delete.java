package net.poundex.dowithpkg.core.rule.rules;

import io.vavr.Predicates;
import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.io.IO;
import net.poundex.dowithpkg.core.rule.PreparedRule;
import net.poundex.dowithpkg.core.rule.Rule;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class Delete implements Rule {

	private final IO io;

	@Override
	public PreparedRule prepare(Map<String, Object> args) {
		return path -> delete(
			path,
			checkArg(args.get("Match"), "Search pattern"));
	}

	private void delete(Path path, String match) {
		Pattern pattern = Pattern.compile(match);
		io.writeString(path,
			io.readString(path).lines()
				.filter(Predicates.not(pattern.asPredicate()))
				.collect(Collectors.joining("\n")));
	}
}
