package net.poundex.dowithpkg.core.rule.rules;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.io.IO;
import net.poundex.dowithpkg.core.rule.PreparedRule;
import net.poundex.dowithpkg.core.rule.Rule;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class ReplaceCommand implements Rule {
	
	private final IO io;
	
	@Override
	public PreparedRule prepare(Map<String, Object> args) {
		return path -> replaceCommand(path, 
			checkArg(args.get("Find"), "Find command pattern"),
			checkArg(args.get("Replace"), "Replacement command"));
	}

	private void replaceCommand(Path path, String find, String replace) {
		String flattened = io.readString(path).replaceAll(
			" *\\\\\n",
			"");

		Pattern pattern = Pattern.compile(find);

		io.writeString(path,
			flattened.lines()
				.map(l -> processLine(pattern, l, replace))
				.collect(Collectors.joining("\n")));
	}

	private String processLine(Pattern pattern, String line, String replace) {
		Matcher matcher = pattern.matcher(line);
		if ( ! matcher.find())
			return line;

		return line.substring(0, matcher.start()) + replace;
	}
}
