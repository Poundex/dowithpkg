package net.poundex.dowithpkg.core.processor;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.io.IO;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

@Service
@RequiredArgsConstructor
class DefaultFileStateService implements FileStateService {

	private final IO io;
	
	@Override
	public void writeFileState(ProcessorResult processorResult) {
		Properties properties = new Properties();
		Path stateFile = processorResult.workDir().resolve(".dowithpkg");
		if (io.exists(stateFile))
			io.withReader(stateFile, r -> {
				properties.load(r);
				return null;
			});

		processorResult.filesProcessed().entrySet().stream()
			.filter(Map.Entry::getValue)
			.map(Map.Entry::getKey)
			.forEach(f -> properties.put(f, io.hash(processorResult.workDir().resolve(f))));

		io.withWriter(stateFile, w -> {
			properties.store(w, null);
			return null;
		});
	}

	@Override
	public DirectoryState getDirectoryState(Path workDir) {
		Path stateFile = workDir.resolve(".dowithpkg");
		if( ! io.exists(stateFile))
			return DirectoryState.create(Collections.emptyMap());
		
		return io.withReader(stateFile, r -> {
			Properties p = new Properties();
			p.load(r);
			return new DirectoryState(p);
		});
	}
}
