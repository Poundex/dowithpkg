package net.poundex.dowithpkg.core;

import java.nio.file.Path;

public record RuntimeConfiguration(Path configDir) {
}
