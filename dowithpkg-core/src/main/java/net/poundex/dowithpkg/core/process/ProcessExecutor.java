package net.poundex.dowithpkg.core.process;

import java.nio.file.Path;

public interface ProcessExecutor {
	void executeUser(String... command);
	void executeTask(Path dir, String... command);
}
