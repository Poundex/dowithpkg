package net.poundex.dowithpkg.cli

import net.poundex.dowithpkg.core.output.Output
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource
import spock.lang.Specification

@SpringBootTest
@TestPropertySource(properties = "dowithpkg.cli.auto-launch=false")
class AbstractFunctionalSpec extends Specification {
	
	@Autowired
	ApplicationLauncher applicationLauncher
	
	StringBuilder output = new StringBuilder()
	
	@SpringBean
	Output _outputStub = Stub() {
		out(_) >> { String s -> output.append("${s}\n") } 
	}
	
	protected void reset() {
		output = new StringBuilder()
	}
}
