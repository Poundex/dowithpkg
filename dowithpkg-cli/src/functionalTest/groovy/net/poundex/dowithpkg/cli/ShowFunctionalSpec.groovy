package net.poundex.dowithpkg.cli


import java.nio.file.Path

class ShowFunctionalSpec extends AbstractFunctionalSpec {
	
	void "Does nothing when no rules for current dir available"() {
		when:
		int xi = applicationLauncher.launch("show")
		
		then:
		xi == 0
		output.empty
	}
	
	void "Errors when explicit ruleset file does not exist"() {
		when:
		int xi = applicationLauncher.launch("show", "--ruleset=fakefile.json")
		
		then:
		xi != 0
	}
	
	void "Invokes diff tool after applying rules"() {
		given:
		Path workDir = TestHelper.generatePackage("simple", "bfr01")
		
		when:
		int xi = applicationLauncher.launch(
			"--X-no-exec",
			"show", 
			"--diff-tool=DiffTool",
			"--ruleset=${workDir.resolve("bfr01.json")}", 
			"--work-dir=${workDir.toAbsolutePath().toString()}")
		
		then:
		xi == 0
		output.toString().startsWith("Exec [DiffTool, ${workDir.toAbsolutePath().toString()}")
	}
}
