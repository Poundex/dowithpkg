package net.poundex.dowithpkg.cli

import groovy.ant.AntBuilder
import org.springframework.core.io.ClassPathResource

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class TestHelper {
	
	private static final AntBuilder ant = new AntBuilder()

	static Path generatePackage(String name, String ruleset) {
		Path p = Files.createTempDirectory("ftest")
		Path srcDir = Paths.get(new ClassPathResource("packages/" + name).getURI())

		ant.copy(todir: p.toAbsolutePath().toString()) {
			fileset(dir: srcDir.toAbsolutePath().toString()) {
				include(name: "**")
			}
		}
		
		Files.write(p.resolve("${ruleset}.json"),
			new ClassPathResource("${ruleset}.json").getContentAsByteArray())

		return p
	}
}
