package net.poundex.dowithpkg.cli


import java.nio.file.Path

class MakepkgFunctionalSpec extends AbstractFunctionalSpec {
	
	void "Only processes once, always invokes makepkg"(List<List<String>> argSet) {
		given:
		Path workDir = TestHelper.generatePackage("simple", "bfr01")

		when:
		argSet.each { args ->
			applicationLauncher.launch(
				"--X-no-exec",
				"makepkg",
				"--ruleset=${workDir.resolve("bfr01.json")}",
				"--work-dir=${workDir.toAbsolutePath().toString()}",
				*args)
		}

		then:
		output.readLines() == [
			"Applied bfr01 to 2/2 file(s)", 
			*(argSet.collect {"Exec ${workDir.toAbsolutePath().toString()} " + [ "makepkg", it ].flatten() })]

		where:
		argSet << [
			[
				["--verifysource", "-Ccf"],
				["--nobuild", "-fC", "--ignorearch"],
				["--packagelist"],
				["-cf", "--noconfirm", "--noextract", "--noprepare", "--holdver", "--ignorearch"],
			],
			[
				["--verifysource", "-Ccf"],
				["-ofA", "-C"],
				["--packagelist"],
				["-feA", "--noconfirm", "--noprepare", "--holdver", "-c"]
			]
		]
	}

}
