package net.poundex.dowithpkg.cli


import java.nio.file.Path

class ApplyFunctionalSpec extends AbstractFunctionalSpec {
	
	void "Does nothing when no rules for current dir available"() {
		when:
		int xi = applicationLauncher.launch("apply")
		
		then:
		xi == 0
		output.empty
	}
	
	void "Errors when explicit ruleset file does not exist"() {
		when:
		int xi = applicationLauncher.launch("apply", "--ruleset=fakefile.json")
		
		then:
		xi != 0
	}
	
	void "Applies rules to directory"() {
		given:
		Path workDir = TestHelper.generatePackage("simple", "bfr01")
		
		when:
		int xi = applicationLauncher.launch(
			"apply", 
			"--ruleset=${workDir.resolve("bfr01.json")}", 
			"--work-dir=${workDir.toAbsolutePath().toString()}")
		
		then:
		xi == 0
		
		and:
		workDir.resolve("PKGBUILD").readLines()*.strip().contains("./configure --prefix=/usr/local")
		workDir.resolve("nested/file").text.strip() == "somefile two"
		
		and:
		output.toString().strip() == "Applied bfr01 to 2/2 file(s)"
	}
	
	void "Does not apply rules to directory when already applied"() {
		given:
		Path workDir = TestHelper.generatePackage("simple", "bfr01")

		when:
		int xi1 = applicationLauncher.launch(
			"apply",
			"--ruleset=${workDir.resolve("bfr01.json")}",
			"--work-dir=${workDir.toAbsolutePath().toString()}")

		then:
		xi1 == 0
		output.toString().strip() == "Applied bfr01 to 2/2 file(s)"

		when:
		reset()
		int xi2 = applicationLauncher.launch(
			"apply",
			"--ruleset=${workDir.resolve("bfr01.json")}",
			"--work-dir=${workDir.toAbsolutePath().toString()}")

		then:
		xi2 == 0
		output.toString().strip() == "Applied bfr01 to 0/2 file(s)"

		and:
		workDir.resolve("PKGBUILD").readLines()*.strip().find { it.contains("--prefix=/usr/local") }
		workDir.resolve("nested/file").text.strip() == "somefile two"
	}
}
