package net.poundex.dowithpkg.cli.command

import net.poundex.dowithpkg.core.output.Output
import net.poundex.dowithpkg.core.processor.DirectoryProcessor
import net.poundex.dowithpkg.core.processor.FileStateService
import net.poundex.dowithpkg.core.processor.ProcessorResult
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.nio.file.Paths

class ApplyCommandSpec extends Specification {
	CommandRunner commandRunner = Stub() {
		runCommand(_ as Runnable) >> { Runnable r -> r.run() }
	}

	Path workDir = Paths.get("/work/dir")

	Output output = Stub()
	ProcessorMixin processorMixin = new ProcessorMixin(Optional.empty(), Optional.of(workDir))
	DirectoryProcessor directoryProcessor = Mock()
	FileStateService fileHashService = Mock()

	@Subject
	ApplyCommand command = new ApplyCommand(
		commandRunner, directoryProcessor, output, processorMixin, fileHashService)

	void "Processes directory and then write result"() {
		given:
		ProcessorResult processorResult = new ProcessorResult(
			workDir,
			Paths.get("/stage/dir"),
			true,
			"???",
			[foo: true])
		
		when:
		command.run()

		then:
		1 * directoryProcessor.processDirectory(Optional.empty(), Optional.of(workDir)) >>
			processorResult

		1 * directoryProcessor.writeProcessed({ ProcessorResult pr ->
			pr.workDir() == Paths.get("/work/dir") &&
				pr.stageDir() == Paths.get("/stage/dir") &&
				pr.processed()
		})
		
		and:
		1 * fileHashService.writeFileState(processorResult)
	}
}
