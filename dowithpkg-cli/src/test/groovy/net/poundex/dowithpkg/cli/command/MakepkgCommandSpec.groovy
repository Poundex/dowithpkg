package net.poundex.dowithpkg.cli.command

import net.poundex.dowithpkg.core.output.Output
import net.poundex.dowithpkg.core.process.ProcessExecutor
import net.poundex.dowithpkg.core.process.ProcessExecutorFactory
import net.poundex.dowithpkg.core.processor.DirectoryProcessor
import net.poundex.dowithpkg.core.processor.FileStateService
import net.poundex.dowithpkg.core.processor.ProcessorResult
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.nio.file.Paths

class MakepkgCommandSpec extends Specification {
	CommandRunner commandRunner = Stub() {
		runCommand(_ as Runnable) >> { Runnable r -> r.run() }
	}

	Path workDir = Paths.get("/work/dir")

	Output output = Stub()
	ProcessorMixin processorMixin = new ProcessorMixin(Optional.empty(), Optional.of(workDir))
	DirectoryProcessor directoryProcessor = Mock()
	FileStateService fileHashService = Mock()
	ProcessExecutor processExecutor = Mock()
	ProcessExecutorFactory processExecutorFactory = Stub() {
		get() >> { processExecutor }
	}
	
	@Subject
	MakepkgCommand command = new MakepkgCommand(
		commandRunner, directoryProcessor, output, processorMixin, fileHashService, processExecutorFactory)
	
	void "Does not run after sources are fetched"(List<String> args) {
		setup:
		command.setMakepkgArgs(args)
		
		when:
		command.run()
		
		then:
		1 * processExecutor.executeTask(workDir, (["makepkg"] + args) as String[])
		
		and:
		0 * directoryProcessor._
		0 * fileHashService._
		
		
		where:
		args << [
		    ["--nobuild", "-fC", "--ignorearch"],
			["--packagelist"],
			["-cf", "--noconfirm", "--noextract", "--noprepare", "--holdver", "--ignorearch"],
			["-ofA", "-C"],
			["-feA", "--noconfirm", "--noprepare", "--holdver", "-c"]
		]
	}

	void "Processes then invokes makepkg"(List<String> args) {
		given:
		command.setMakepkgArgs(args)
		
		ProcessorResult processorResult = new ProcessorResult(
			workDir,
			Paths.get("/stage/dir"),
			true,
			"???",
			[foo: true])

		when:
		command.run()

		then:
		1 * directoryProcessor.processDirectory(Optional.empty(), Optional.of(workDir)) >>
			processorResult

		1 * directoryProcessor.writeProcessed({ ProcessorResult pr ->
			pr.workDir() == Paths.get("/work/dir") &&
				pr.stageDir() == Paths.get("/stage/dir") &&
				pr.processed()
		})

		and:
		1 * fileHashService.writeFileState(processorResult)
		
		and:
		1 * processExecutor.executeTask(workDir, (["makepkg"] + args) as String[])
		
		where:
		args << [
		    ["--verifysource"],
			["--verifysource", "-Ccf"]
		]
	}
}
