package net.poundex.dowithpkg.cli.command

import net.poundex.dowithpkg.core.output.Output
import net.poundex.dowithpkg.core.process.ProcessExecutor
import net.poundex.dowithpkg.core.process.ProcessExecutorFactory
import net.poundex.dowithpkg.core.processor.DirectoryProcessor
import net.poundex.dowithpkg.core.processor.ProcessorResult
import spock.lang.Specification
import spock.lang.Subject

import java.nio.file.Path
import java.nio.file.Paths

class ShowCommandSpec extends Specification {
	
	CommandRunner commandRunner = Stub() {
		runCommand(_ as Runnable) >> { Runnable r -> r.run() } 
	}
	
	Path workDir = Paths.get("/work/dir")
	
	Output output = Stub()
	ProcessorMixin processorMixin = new ProcessorMixin(Optional.empty(), Optional.of(workDir))
	DirectoryProcessor directoryProcessor = Mock()
	ProcessExecutor processExecutor = Mock()
	ProcessExecutorFactory processExecutorFactory = Stub() {
		get() >> { processExecutor }
	}
	
	@Subject
	ShowCommand command = new ShowCommand(
		commandRunner, directoryProcessor, output, processorMixin, processExecutorFactory)
	
	void "Processes directory and then shows diff"() {
		when:
		command.run()
		
		then:
		1 * directoryProcessor.processDirectory(Optional.empty(), Optional.of(workDir)) >> 
			new ProcessorResult(workDir, Paths.get("/stage/dir"), true, "???", ["foo": true])
		
		1 * processExecutor.executeUser(
			"git", "diff", "--diff-filter=AM", "/work/dir", "/stage/dir")
	}
}
