package net.poundex.dowithpkg.cli.command

import org.springframework.boot.logging.LogLevel
import org.springframework.boot.logging.LoggingSystem
import spock.lang.Specification
import spock.lang.Subject

class CommandRunnerSpec extends Specification {
	
	RootCommand rootCommand = Stub()
	LoggingSystem loggingSystem = Mock()
	
	@Subject
	CommandRunner commandRunner = new CommandRunner(loggingSystem, rootCommand)
	
	void "Sets log level (default)"() {
		given:
		rootCommand.getVerbose() >> []

		when:
		commandRunner.runCommand({})

		then:
		1 * loggingSystem.setLogLevel(LoggingSystem.ROOT_LOGGER_NAME, LogLevel.ERROR)
	}
	
	void "Sets log level (-v)"() {
		given:
		rootCommand.getVerbose() >> [true]
		
		when:
		commandRunner.runCommand({})
		
		then:
		1 * loggingSystem.setLogLevel("net.poundex.dowithpkg", LogLevel.INFO)
	}
	
	void "Sets log level (-vv)"() {
		given:
		rootCommand.getVerbose() >> [true, true]

		when:
		commandRunner.runCommand({})

		then:
		1 * loggingSystem.setLogLevel(LoggingSystem.ROOT_LOGGER_NAME, LogLevel.INFO)
		1 * loggingSystem.setLogLevel("net.poundex.dowithpkg", LogLevel.DEBUG)
	}
	
	void "Runs command"() {
		given:
		Runnable runnable = Mock()
		
		when:
		commandRunner.runCommand(runnable)
		
		then:
		1 * runnable.run()
	}
}
