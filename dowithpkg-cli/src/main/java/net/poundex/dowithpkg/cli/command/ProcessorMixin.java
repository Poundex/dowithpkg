package net.poundex.dowithpkg.cli.command;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.nio.file.Path;
import java.util.Optional;

@Component
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProcessorMixin {
	@CommandLine.Option(names = {"--ruleset"}, description = /* TODO */ "Ruleset description")
	private Optional<String> ruleset;

	@CommandLine.Option(names = {"--work-dir"}, description = /* TODO */ "Work Dir description")
	private Optional<Path> workDir;
}
