package net.poundex.dowithpkg.cli.command;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.logging.LogLevel;
import org.springframework.boot.logging.LoggingSystem;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
class CommandRunner {
	
	private final LoggingSystem loggingSystem;
	private final RootCommand rootCommand;
	
	void runCommand(Runnable runnable) {
		setLogLevel();
		runnable.run();
	}
	
	private void setLogLevel() {
		if(rootCommand.getVerbose().length >= 2 && rootCommand.getVerbose()[1]) {
			loggingSystem.setLogLevel(LoggingSystem.ROOT_LOGGER_NAME, LogLevel.INFO);
			loggingSystem.setLogLevel("net.poundex.dowithpkg", LogLevel.DEBUG);
		}
		else if(rootCommand.getVerbose().length == 1 && rootCommand.getVerbose()[0]) {
			loggingSystem.setLogLevel("net.poundex.dowithpkg", LogLevel.INFO);
		}
		else {
			loggingSystem.setLogLevel(LoggingSystem.ROOT_LOGGER_NAME, LogLevel.ERROR);
		}
	}
}
