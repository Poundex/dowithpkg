package net.poundex.dowithpkg.cli.command;

import lombok.RequiredArgsConstructor;
import net.poundex.dowithpkg.core.output.Output;
import net.poundex.dowithpkg.core.processor.DirectoryProcessor;
import net.poundex.dowithpkg.core.processor.ProcessorResult;
import picocli.CommandLine;

@RequiredArgsConstructor
abstract class AbstractProcessDirectoryCommand implements Runnable {
	
	private final CommandRunner commandRunner;

	protected final DirectoryProcessor directoryProcessor;
	protected final Output output;
	
	@CommandLine.Mixin
	protected final ProcessorMixin processorMixin;

	@Override
	public void run() {
		commandRunner.runCommand(() -> {
			ProcessorResult processorResult = directoryProcessor.processDirectory(
				processorMixin.getRuleset(),
				processorMixin.getWorkDir());

			if( ! processorResult.processed())
				return;
			
			afterDirectoryProcessed(processorResult);
		});
	}

	protected abstract void afterDirectoryProcessed(ProcessorResult processorResult);
}
