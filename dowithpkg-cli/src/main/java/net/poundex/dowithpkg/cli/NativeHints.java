package net.poundex.dowithpkg.cli;

import org.springframework.aot.hint.MemberCategory;
import org.springframework.aot.hint.RuntimeHints;
import org.springframework.aot.hint.RuntimeHintsRegistrar;
import org.springframework.boot.logging.java.SimpleFormatter;
import org.springframework.core.io.ClassPathResource;

public class NativeHints implements RuntimeHintsRegistrar {
	@Override
	public void registerHints(RuntimeHints hints, ClassLoader classLoader) {
		hints.resources().registerResource(new ClassPathResource("version.txt"));
		hints.resources().registerResource(new ClassPathResource("logging.properties"));
		hints.reflection().registerType(SimpleFormatter.class, MemberCategory.INVOKE_PUBLIC_CONSTRUCTORS);
	}
}
