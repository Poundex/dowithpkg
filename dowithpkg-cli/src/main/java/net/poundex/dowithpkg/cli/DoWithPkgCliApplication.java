package net.poundex.dowithpkg.cli;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportRuntimeHints;

@SpringBootApplication
@ImportRuntimeHints(NativeHints.class)
@RequiredArgsConstructor
@ComponentScan(basePackages = "net.poundex.dowithpkg")
public class DoWithPkgCliApplication {
	
	public static void main(String[] args) {
		System.exit(SpringApplication.exit(SpringApplication.run(DoWithPkgCliApplication.class, args)));
	}
}
