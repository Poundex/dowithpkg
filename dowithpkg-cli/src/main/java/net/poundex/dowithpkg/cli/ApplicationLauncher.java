package net.poundex.dowithpkg.cli;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

@Component
@RequiredArgsConstructor
class ApplicationLauncher implements CommandLineRunner, ExitCodeGenerator {
	
	private final Environment environment;
	private final CommandLine commandLine;
	
	private int exitCode;
	
	@Override
	public void run(String[] args) {
		if(environment.getProperty("dowithpkg.cli.auto-launch", boolean.class, true))
			launch(args);
	}

	public int launch(String... args) {
		this.exitCode = commandLine.execute(args);
		return this.exitCode;
	}

	@Override
	public int getExitCode() {
		return exitCode;
	}
}
