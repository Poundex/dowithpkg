package net.poundex.dowithpkg.cli.command;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.poundex.dowithpkg.core.output.Output;
import net.poundex.dowithpkg.core.process.ProcessExecutorFactory;
import net.poundex.dowithpkg.core.processor.DirectoryProcessor;
import net.poundex.dowithpkg.core.processor.ProcessorResult;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.util.Arrays;
import java.util.stream.Stream;

@CommandLine.Command(
	name = "show", 
	description = /* TODO */ "Show description",
	mixinStandardHelpOptions = true)
@Slf4j
@Setter
@Component
public class ShowCommand extends AbstractProcessDirectoryCommand {
	
	private final ProcessExecutorFactory processExecutorFactory;

	@CommandLine.Option(names = "--diff-tool", defaultValue = "git diff --diff-filter=AM")
	private String diffTool = "git diff --diff-filter=AM"; // TODO ?

	public ShowCommand(CommandRunner commandRunner, DirectoryProcessor directoryProcessor, Output output, ProcessorMixin processorMixin, ProcessExecutorFactory processExecutorFactory) {
		super(commandRunner, directoryProcessor, output, processorMixin);
		this.processExecutorFactory = processExecutorFactory;
	}

	@Override
	protected void afterDirectoryProcessed(ProcessorResult processorResult) {
		String usedWorkDir = processorResult.workDir().toAbsolutePath().toString();
		String usedStageDir = processorResult.stageDir().toAbsolutePath().toString();

		processExecutorFactory.get().executeUser(
			Stream.concat(
					Arrays.stream(diffTool.split(" ")),
					Stream.of(usedWorkDir, usedStageDir))
				.toArray(String[]::new));
	}
}
