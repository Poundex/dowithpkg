package net.poundex.dowithpkg.cli.command;

import net.poundex.dowithpkg.core.output.Output;
import net.poundex.dowithpkg.core.processor.DirectoryProcessor;
import net.poundex.dowithpkg.core.processor.FileStateService;
import net.poundex.dowithpkg.core.processor.ProcessorResult;

abstract class AbstractStatefulProcessCommand extends AbstractProcessDirectoryCommand {

	protected final FileStateService fileStateService;
	
	public AbstractStatefulProcessCommand(
		CommandRunner commandRunner, DirectoryProcessor directoryProcessor, Output output, ProcessorMixin processorMixin, FileStateService fileStateService) {
		super(commandRunner, directoryProcessor, output, processorMixin);
		this.fileStateService = fileStateService;
	}
	
	@Override
	protected void afterDirectoryProcessed(ProcessorResult processorResult) {
		directoryProcessor.writeProcessed(processorResult);

		output.out(String.format("Applied %s to %s/%s file(s)",
			processorResult.rulesetName(),
			processorResult.filesProcessed().values().stream().filter(x -> x).count(),
			processorResult.filesProcessed().size()));

		fileStateService.writeFileState(processorResult);
	}
}
