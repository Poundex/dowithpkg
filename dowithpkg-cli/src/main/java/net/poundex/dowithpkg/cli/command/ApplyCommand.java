package net.poundex.dowithpkg.cli.command;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.poundex.dowithpkg.core.output.Output;
import net.poundex.dowithpkg.core.processor.DirectoryProcessor;
import net.poundex.dowithpkg.core.processor.FileStateService;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

@CommandLine.Command(
	name = "apply", 
	description = /* TODO */ "Apply description",
	mixinStandardHelpOptions = true)
@Slf4j
@Setter
@Component
public class ApplyCommand extends AbstractStatefulProcessCommand {
	
	public ApplyCommand(
		CommandRunner commandRunner, DirectoryProcessor directoryProcessor, Output output, ProcessorMixin processorMixin, FileStateService fileStateService) {
		super(commandRunner, directoryProcessor, output, processorMixin, fileStateService);
	}
}
