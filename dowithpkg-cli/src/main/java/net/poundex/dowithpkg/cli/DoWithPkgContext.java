package net.poundex.dowithpkg.cli;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.poundex.dowithpkg.cli.command.RootCommand;
import net.poundex.dowithpkg.core.output.Output;
import net.poundex.dowithpkg.core.RuntimeConfiguration;
import net.poundex.dowithpkg.core.exception.ClientException;
import net.poundex.dowithpkg.core.process.ProcessExecutorFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import picocli.CommandLine;

import java.nio.file.Paths;

@Configuration
class DoWithPkgContext {
	@Bean
	public RuntimeConfiguration runtimeConfiguration(RootCommand rootCommand) {
		return new RuntimeConfiguration(rootCommand.getConfigDir()
			.orElse(Paths.get(System.getenv("HOME"))
				.resolve(".config/dowithpkg/")));
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
	
	@Bean
	public CommandLine commandLine(CommandLine.IFactory cliFactory) {
		CommandLine commandLine = new CommandLine(RootCommand.class, cliFactory);
		commandLine.setCommandName("dowithpkg");
		commandLine.getCommandSpec().exitCodeOnUsageHelp(CommandLine.ExitCode.USAGE);
		commandLine.getCommandSpec().exitCodeOnVersionHelp(CommandLine.ExitCode.USAGE);
		CommandLine.IExecutionExceptionHandler executionExceptionHandler =
			commandLine.getExecutionExceptionHandler();
		commandLine.setExecutionExceptionHandler((ex, commandLine1, parseResult) -> {
			if(ex instanceof ClientException cex) {
				commandLine1.getErr().println(cex.getMessage());
				return 1;
			}
			return executionExceptionHandler.handleExecutionException(ex, commandLine1, parseResult);
		});
		return commandLine;
	}
	
	@Bean
	ProcessExecutorFactory processExecutorFactory(RootCommand rootCommand, Output output) {
		return new ProcessExecutorFactory(rootCommand::isNoExec, output);
	}
}
