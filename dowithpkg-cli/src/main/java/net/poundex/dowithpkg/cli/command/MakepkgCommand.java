package net.poundex.dowithpkg.cli.command;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.poundex.dowithpkg.core.output.Output;
import net.poundex.dowithpkg.core.process.ProcessExecutorFactory;
import net.poundex.dowithpkg.core.processor.DirectoryProcessor;
import net.poundex.dowithpkg.core.processor.FileStateService;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import picocli.CommandLine;

import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

@CommandLine.Command(
	name = "makepkg", 
	description = /* TODO */ "Makepkg description",
	hidden = true)
@Slf4j
@Setter
@Component
public class MakepkgCommand extends AbstractStatefulProcessCommand {
	
	private final ProcessExecutorFactory processExecutorFactory;
	
	@CommandLine.Unmatched
	private List<String> makepkgArgs;

	public MakepkgCommand(CommandRunner commandRunner, DirectoryProcessor directoryProcessor, Output output, ProcessorMixin processorMixin, FileStateService fileStateService, ProcessExecutorFactory processExecutorFactory) {
		super(commandRunner, directoryProcessor, output, processorMixin, fileStateService);
		this.processExecutorFactory = processExecutorFactory;
	}

	@Override
	public void run() {
		if ( ! CollectionUtils.isEmpty(makepkgArgs) && makepkgArgs.contains("--verifysource")) 
			super.run();
		
		invokeMakepkg();
	}

	private void invokeMakepkg() {
		processExecutorFactory.get().executeTask(
			processorMixin.getWorkDir().orElse(Paths.get(System.getProperty("user.dir"))), Stream.concat(
					Stream.of("makepkg"),
					makepkgArgs.stream())
				.toArray(String[]::new));
	}
}
