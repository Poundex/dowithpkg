package net.poundex.dowithpkg.cli.command;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import picocli.CommandLine;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Optional;

@CommandLine.Command(
	versionProvider = RootCommand.VersionProvider.class, 
	subcommands = { ApplyCommand.class, ShowCommand.class, MakepkgCommand.class },
	mixinStandardHelpOptions = true, 
	description = /* TODO */ "Tool Description")
@Component
@Slf4j
@Setter
public class RootCommand {
	
	@CommandLine.Option(
		names = {"-c", "--config-dir"},
		description = /* TODO */ "Config Dir Description",
		showDefaultValue = CommandLine.Help.Visibility.ALWAYS,
		defaultValue = "~/.config/dowithpkg/")
	@Getter
	private Optional<Path> configDir = Optional.empty();
	
	@CommandLine.Option(names = "--X-no-exec", hidden = true, defaultValue = "false")
	@Getter
	private boolean noExec;
	
	@CommandLine.Option(names = {"-v"}, description = /* TODO */ "Verbose description (-vv more description)")
	@Getter
	private boolean[] verbose = new boolean[] { };
	
	static class VersionProvider implements CommandLine.IVersionProvider {
		@Override
		public String[] getVersion() throws Exception {
			return new String[]{
				StreamUtils.copyToString(
					new ClassPathResource("version.txt").getInputStream(),
					StandardCharsets.UTF_8)};
		}
	}
}
